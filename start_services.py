#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved

import os
import sys
import json
import logging
from http.server import HTTPServer, BaseHTTPRequestHandler

current_dir = os.path.dirname(os.path.realpath(__file__))

# =========================================
# FastText
# =========================================
import fasttext
sys.stderr.write('Loading FasText models ... ')
model_100 = fasttext.load_model(f'{current_dir}/fasttext/models/cstenten_17_100_5_5_0.05_skip')
# model_300 = fasttext.load_model(f'{current_dir}/fasttext/models/cstenten_17_300_5_5_0.05_skip')
# model_500 = fasttext.load_model(f'{current_dir}/fasttext/models/cstenten_17_500_5_5_0.05_skip')
sys.stderr.write('loaded\n')

# =========================================
# Answer selection
# =========================================
sys.path.append('{}/answer_selection_bert/'.format(current_dir))
from answer_selection_bert.answer_selection import AnswerSelection
sys.stderr.write('Loading answer selection model ... ')
sqad_db = "/nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database/sqad_db/stable"
as_model = "/nlp/projekty/question_answering/AQA_v2/sqad_tools/bert_AS/bert_as_torch/scripts/best_model_dir"
ans_sel = AnswerSelection(as_model, '', '', sqad_db, batch_size=10)
sys.stderr.write('loaded\n')

# =========================================
# Answer extraction ML
# =========================================
from transformers import pipeline
from transformers import AutoTokenizer
from transformers import AutoModelForQuestionAnswering
sys.stderr.write('Loading answer extraction ML model ...')
answer_extraction_ml_model_path = f"{current_dir}/answer_extraction/ml_extraction/train_2022_08_16_17-19-20/xlm-roberta-large-squad2-finetuned-V1/"
answer_extraction_ml_tokenizer = AutoTokenizer.from_pretrained(answer_extraction_ml_model_path)
answer_extraction_ml_tokenizer.padding_side = "right"
answer_extraction_ml_model = AutoModelForQuestionAnswering.from_pretrained(answer_extraction_ml_model_path)
answer_extraction_ml_pipeline = pipeline('question-answering', model=answer_extraction_ml_model,
                                         tokenizer=answer_extraction_ml_tokenizer)
sys.stderr.write('loaded\n')

# =========================================
# Answer extraction Yes/No
# =========================================
from transformers import AutoModelForSequenceClassification
sys.stderr.write('Loading answer extraction ML-YES_NO model ... ')
id2label = {'LABEL_0': 'no', 'LABEL_1': 'yes'}
answer_extraction_ml_yes_no_model_path = f'{current_dir}/answer_extraction/ml_yes_no/train_2022_09_19_17-38-32/xlm-roberta-large-squad2-finetuned-V1/'
answer_extraction_ml_yes_no_tokenizer = AutoTokenizer.from_pretrained(answer_extraction_ml_yes_no_model_path)
answer_extraction_ml_yes_no_tokenizer.padding_side = "right"
answer_extraction_ml_yes_no_model = AutoModelForSequenceClassification.from_pretrained(answer_extraction_ml_yes_no_model_path)
answer_extraction_ml_yes_no_classify = pipeline('text-classification', model=answer_extraction_ml_yes_no_model,
                                                tokenizer=answer_extraction_ml_yes_no_tokenizer)

# =========================================
# Question classifier yes/no, rest
# =========================================
sys.stderr.write('Loading question classifier (yes/no, rest) model ... ')
id2label_2 = {'LABEL_0': 'REST', 'LABEL_1': 'YES_NO'}
question_classification_model_path = f'{current_dir}/question_classification_v2/train_2022_10_24_18-20-18/xlm-roberta-large-squad2-finetuned-V1/'
question_classification_tokenizer = AutoTokenizer.from_pretrained(question_classification_model_path)
question_classification_tokenizer.padding_side = "right"
question_classification_model = AutoModelForSequenceClassification.from_pretrained(question_classification_model_path)
question_classification_classify = pipeline('text-classification', model=question_classification_model,
                                            tokenizer=question_classification_tokenizer)
sys.stderr.write('loaded\n')

sys.stderr.write('Service ready.\n')


class JobServer(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def _get_data(self):
        content_length = int(self.headers['Content-Length'])
        return json.loads(self.rfile.read(content_length))

    def do_POST(self):
        command = self.path[1:]
        data = self._get_data()
        try:

            if command == 'fasttext':
                method = getattr(self, data["size"])
                result = method(data["sentence"])

                logging.info(f'POST {command}; {data["size"]}, {data["sentence"]}')
                self._set_headers()
                self.wfile.write(json.dumps({'command': command,
                                             'size': data["size"],
                                             'sentence': data["sentence"],
                                             'vectors': result}).encode('utf-8'))

            elif command == 'answer_selection':
                question, as_answer, doc_url = ans_sel.answer_selection(data['question'], data['doc_id'])

                logging.info(f'POST {command}; {data["question"]}, {data["doc_id"]}')
                self._set_headers()
                self.wfile.write(json.dumps({'command': command,
                                             'question': question,
                                             'doc_id': data["doc_id"],
                                             'answer_list': as_answer,
                                             'doc_url': doc_url}).encode('utf-8'))

            elif command == 'answer_extraction':
                qa_input = {'question': data['question'],
                            'context': data['answer_selection']}

                prediction = answer_extraction_ml_pipeline(qa_input)

                logging.info(f'POST {command}; {data["question"]}, {data["answer_selection"]}')
                self._set_headers()
                self.wfile.write(json.dumps({'command': command,
                                             'question': data['question'],
                                             'answer_selection': data["answer_selection"],
                                             'answer_extraction': prediction["answer"],
                                             'answer_extraction_score': prediction["score"]}).encode('utf-8'))
            elif command == 'yes_no':
                query_input = {
                    'text': f'{data["question"]} [SEP] {data["answer_selection"]}'
                }

                prediction = answer_extraction_ml_yes_no_classify(query_input)

                logging.info(f'POST {command}; {data["question"]}, {data["answer_selection"]}')
                self._set_headers()
                self.wfile.write(json.dumps({'command': command,
                                             'question': data['question'],
                                             'answer_selection': data["answer_selection"],
                                             'answer_extraction': id2label[prediction["label"]],
                                             'answer_extraction_score': prediction["score"]}).encode('utf-8'))

            elif command == 'q_type_2':
                query_input = {
                    'text': f'{data["question"]}'
                }
                translate_type = {'LABEL_0': 'REST', 'LABEL_1': 'YES_NO'}

                q_type = translate_type[question_classification_classify(query_input)['label']]

                logging.info(f'POST {command}; {data["question"]}')
                self._set_headers()
                self.wfile.write(json.dumps({'command': command,
                                             'question': data['question'],
                                             'type': q_type}).encode('utf-8'))
        except Exception as e:
            sys.stderr.write(f'ERROR: someting went wrong. {e}\n')

    @staticmethod
    def v100(sentence):
        data = []
        for w in sentence.strip().split(' '):
            data.append((w, list([float(x) for x in model_100[w]])))
        return data

    # @staticmethod
    # def v300(sentence):
    #     data = []
    #     for w in sentence.strip().split(' '):
    #         data.append((w, list([float(x) for x in model_300[w]])))
    #     return data
    #
    # @staticmethod
    # def v500(sentence):
    #     data = []
    #     for w in sentence.strip().split(' '):
    #         data.append((w, list([float(x) for x in model_500[w]])))
    #     return data


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Service for fasttext, answer selection and answer extraction')
    parser.add_argument('-p', '--port', type=int,
                        required=False, default=4450,
                        help='Port')
    parser.add_argument('-s', '--server', type=str,
                        required=False, default='0.0.0.0',
                        help='Server')
    args = parser.parse_args()

    logging.basicConfig(filename=f'{current_dir}/service.log',
                        filemode='a',
                        format='%(asctime)s|%(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.DEBUG)

    logging.info(f'Starting httpd on port {args.port}...')
    httpd = HTTPServer((args.server, args.port), JobServer)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == "__main__":
    main()
