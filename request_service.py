#! /usr/bin/env python3
# coding: utf-8
import requests
import json

url = 'http://apollo.fi.muni.cz'


def get_vector(sentence, dim=100):
    """
    Get vector representation of ech word
    :param sentence:
    :return:
    """
    r = requests.post(f'{url}:4450/fasttext', data=json.dumps({'sentence': f'{sentence}', 'size': f'v{dim}'}))
    return json.loads(r.content)


def get_answer_extraction_ml(question, answer_selection):
    """
    Get answer extraction
    :param question: str
    :param answer_selection: str
    :return:
    """
    r = requests.post(f'{url}:4450/answer_extraction', data=json.dumps({'question': f'{question}', 'answer_selection': f'{answer_selection}'}))
    return json.loads(r.content)


def get_answer_extraction_ml_yes_no(question, answer_selection):
    """
    Get yes/no answer
    :param question: str
    :param answer_selection: str
    :return:
    """
    r = requests.post(f'{url}:4450/yes_no', data=json.dumps({'question': f'{question}', 'answer_selection': f'{answer_selection}'}))
    return json.loads(r.content)


def get_answer_selection(question, doc_id):
    """
    Get answer selection senteces
    :param question: str
    :param doc_id: str
    :return:
    """
    r = requests.post(f'{url}:4450/answer_selection', data=json.dumps({'question': f'{question}', 'doc_id': f'{doc_id}'}))
    return json.loads(r.content)


def get_document_selection(question, num):
    """
    Get answer selection senteces
    :param question: str
    :param num: int
    :return:
    """
    r = requests.post(f'{url}:4451/document_selection', data=json.dumps({'question': f'{question}', 'num': f'{num}'}))
    return json.loads(r.content)


def get_qa_type(question):
    """
    Get answer selection senteces
    :param question: str
    :return:
    """
    r = requests.post(f'{url}:4451/qa_type', data=json.dumps({'question': f'{question}'}))
    return json.loads(r.content)


def get_q_type_2(question):
    """
    Get answer selection senteces
    :param question: str
    :return:
    """
    r = requests.post(f'{url}:4450/q_type_2', data=json.dumps({'question': f'{question}'}))
    return json.loads(r.content)
