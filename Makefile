CONDA_ACTIVATE=source $$(conda info --base)/etc/profile.d/conda.sh ; conda activate ; conda activate

test:
	echo "Kdo je autorem novely Létající jaguár ?"| ./corpus_tools/majka_czech/majka-czech_v2.sh | ./corpus_tools/vert2wl.py | ./aqa_v2.py

test_yes_no:
	echo "Je olovo toxický prvek ?"| ./corpus_tools/majka_czech/majka-czech_v2.sh | ./corpus_tools/vert2wl.py | ./aqa_v2.py

test_json:
	echo "Kdo je autorem novely Létající jaguár ?"| ./corpus_tools/majka_czech/majka-czech_v2.sh | ./corpus_tools/vert2wl.py | ./aqa_v2.py --output_type json


start_services:
	($(CONDA_ACTIVATE) aqa-apollo; start_services.py
start_services_old:
	($(CONDA_ACTIVATE) base; start_services_old.py
