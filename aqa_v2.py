#! /usr/bin/env python3
# coding: utf-8
import sys
import os
import json
from request_service import get_answer_selection
from request_service import get_answer_extraction_ml
from request_service import get_answer_extraction_ml_yes_no
from request_service import get_document_selection
# from request_service import get_qa_type
from request_service import get_q_type_2


def print_basic_data(data, output):
    output.write(f"Question: {data['question']}\n")
    output.write(f"Question type: {data['q_type']}\n")
    output.write(f"Answer type: {data['a_type']}\n")
    output.write(f"Best documents: \n")
    for score, doc in data.get('ds_result', []):
        output.write(f"{score:.4f}: {doc}\n")
    output.write(f"Selected answer: \n")
    for score, answer in data.get('as_result', [])[:1]:
        output.write(f"{score:.4f}: {'|'.join(answer)}\n")
    output.write(f"Answer extraction: \n")
    for score, answer in data.get('ans_ext', []):
        output.write(f"{score:.4f}: {answer}\n")


def main():
    import argparse
    parser = argparse.ArgumentParser(description='AQA v2 pipeline')
    parser.add_argument('-q', '--question', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input question')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('--output_type', type=str,
                        required=False, default='basic',
                        help='Output format. Supported: json, basic')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False, default=False,
                        help='Debug print')
    # Document selection
    parser.add_argument('--ds_best_n', type=int,
                        required=False, default=1,
                        help='Number of best scored documents to search within')

    args = parser.parse_args()

    try:
        if not os.isatty(args.question.fileno()):
            input_w_question, input_l_question = args.question.read().split('|||')
        else:
            input_w_question = ''
            input_l_question = ''

        # Document selection
        top_n_docs = get_document_selection(input_l_question, args.ds_best_n)['doc']

        if args.verbose:
            print(f'Document selection: {top_n_docs}')

        # Question type
        # q_type, a_type = 'VERB_PHR', 'YES_NO'
        # q_type, a_type = get_qa_type(input_l_question)['types'].split('; ')
        q_type = get_q_type_2(input_l_question)['type']
        a_type = ''

        if args.verbose:
            print(f'q_type: {q_type}')

        # Answer selection
        ans_sel = get_answer_selection(input_w_question, top_n_docs[0][1])

        if args.verbose:
            print(f'ans_sel: {ans_sel}')

        # Answer extraction
        # if a_type == 'YES_NO':
        if q_type == 'YES_NO':
            ans_ext = get_answer_extraction_ml_yes_no(input_w_question, ans_sel['answer_list'][0][1])
        else:
            ans_ext = get_answer_extraction_ml(input_w_question, ' '.join(ans_sel['answer_list'][0][1]))

        data = {}
        data['question'] = input_w_question
        data['ds_result'] = top_n_docs
        data['q_type'] = q_type
        data['a_type'] = a_type
        data['as_result'] = ans_sel['answer_list']
        data['doc_url'] = ans_sel['doc_url']
        data['ans_ext'] = [(ans_ext['answer_extraction_score'], ans_ext['answer_extraction'])]

        if args.output_type == 'json':
            json_string = json.dumps(data, ensure_ascii=False, indent=4).encode('utf8').decode('utf-8')
            args.output.write(f"{json_string}\n")
        elif args.output_type == 'basic':
            print_basic_data(data, args.output)
        else:
            args.output('Not supported output type!\n')
    except Exception as e:
        sys.stderr.write(f'{e}\nDid you start fasttext service? (fasttext/fasttext_service.py)\n')


if __name__ == "__main__":
    main()
