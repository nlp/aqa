#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved

import os
import sys
import json
import logging
from http.server import HTTPServer, BaseHTTPRequestHandler

current_dir = os.path.dirname(os.path.realpath(__file__))

# =========================================
# Document selection
# =========================================
sys.path.append('{}/document_selection/scripts/'.format(current_dir))
from document_selection.scripts.doc_selection import DocumentSelection
sys.stderr.write('Loading document selection model ... ')
ds_model = "/nlp/projekty/question_answering/AQA_v2/document_selection/database/sqad_v3_2022-12-20_11_59_24//"
ds_weight = 0.26
ds = DocumentSelection(ds_model, ds_weight)
sys.stderr.write('loaded\n')

# =========================================
# Question/Answer type classification
# =========================================
sys.path.append('{}/question_classification/'.format(current_dir))
from question_classification.classify_question_lstm_with_fasttext import QAType
sys.stderr.write('Loading Q/A type model ... ')
qa_type_model = '/nlp/projekty/question_answering/AQA_v2/question_classification/classifier/lstm/snapshots/2020-01-20_17-33-31/best_steps_3950_acc_84.21768951416016.pt'
qa_type = QAType(qa_type_model)
sys.stderr.write('loaded\n')

sys.stderr.write('Service ready.\n')


class JobServer(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def _get_data(self):
        content_length = int(self.headers['Content-Length'])
        return json.loads(self.rfile.read(content_length))

    def do_POST(self):
        command = self.path[1:]
        data = self._get_data()
        try:

            if command == 'document_selection':
                top_n_docs = []
                for top_n, _ in ds.doc_selection(data['question'], int(data['num'])):
                    for doc_id in top_n:
                        top_n_docs.append((doc_id[2], f"{doc_id[1]}"))

                logging.info(f'POST {command}; {data["question"]}, {data["num"]}')
                self._set_headers()
                self.wfile.write(json.dumps({'command': command,
                                             'num': data["num"],
                                             'question': data["question"],
                                             'doc': top_n_docs}).encode('utf-8'))
            elif command == 'qa_type':
                types = qa_type.classify(data['question'])

                logging.info(f'POST {command}; {data["question"]}')
                self._set_headers()
                self.wfile.write(json.dumps({'command': command,
                                             'question': data["question"],
                                             'types': types}).encode('utf-8'))
            else:
                sys.stderr.write('ERROR: command not supported.')
        except Exception as e:
            sys.stderr.write(f'ERROR: something went wrong. {e}\n')


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Service document selection and question/answer type classification')
    parser.add_argument('-p', '--port', type=int,
                        required=False, default=4451,
                        help='Port')
    parser.add_argument('-s', '--server', type=str,
                        required=False, default='0.0.0.0',
                        help='Server')
    args = parser.parse_args()

    logging.basicConfig(filename=f'{current_dir}/service_old.log',
                        filemode='a',
                        format='%(asctime)s|%(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.DEBUG)

    logging.info(f'Starting httpd on port {args.port}...')
    httpd = HTTPServer((args.server, args.port), JobServer)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == "__main__":
    main()
