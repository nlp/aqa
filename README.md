# Automatic Question Answering (AQA)
The Question Answering pipeline is built for flective languages. This pipeline comprises four major components: the Question Processor, Document Selector, Answer Selector, and Answer Extractor, where each module incorporates and state-of-the-art algorithm for a specific task. Using a Long Short Term (LSTM) neural network and standard Natural Language Processing (NLP) tools, the Question Processor is able to extract all necessary information regarding the input question. Provided features are then used by Document Selector, which incorporates the TF-IDF scoring mechanism enhanced by our weighting strategy. The final answer is then established by combining the last two modules. First, the Answer Selection module, based on Transformers and reinforced by context features, selects the most probable answer sentence. Then the Answer Extraction module extracts the shortest and still informative span as a final answer.

The final pipeline is tested against the Simple Question Answering Database ([SQAD](https://gitlab.fi.muni.cz/nlp/sqad)) that was developed for training and testing purposes of the AQA system. 

On the SQAD database the Document Selector reaches more than 90\% accuracy on the document selection task, the Answer Selector selects the correct answer sentence in nearly 92\% of cases, and the Answer Extractor provides the correct final answer in almost 91\% of cases. 

AQA have been developed as a part of dissertation in  **NLP laboratory at Masaryk University**.

## Cloning AQA modules
```
git submodule init
git submodule update
```
 
Please cite:
* MEDVEĎ, Marek and Aleš HORÁK. Sentence and Word Embedding Employed in Open Question-Answering. In Proceedings of the 10th International Conference on Agents and Artificial Intelligence (ICAART 2018). Setúbal, Portugal: SCITEPRESS - Science and Technology Publications, 2018. p. 486-492. ISBN 978-989-758-275-2.
```
@inproceedings{1393609,
   author = {Medveď, Marek and Horák, Aleš},
   address = {Setúbal, Portugal},
   booktitle = {Proceedings of the 10th International Conference on Agents and Artificial Intelligence (ICAART 2018)},
   keywords = {question answering; word embedding; word2vec; AQA; Simple Question Answering Database; SQAD},
   howpublished = {print},
   language = {eng},
   location = {Setúbal, Portugal},
   isbn = {978-989-758-275-2},
   pages = {486-492},
   publisher = {SCITEPRESS - Science and Technology Publications},
   title = {Sentence and Word Embedding Employed in Open Question-Answering},
   year = {2018}
}
```

* MEDVEĎ, Marek a Aleš HORÁK. AQA: Automatic Question Answering System for Czech. In Sojka Petr, Horák Aleš, Kopeček Ivan, Pala Karel. Text, Speech, and Dialogue 19th International Conference, TSD 2016 Brno, Czech Republic, September 12–16, 2016 Proceedings. Switzerland: Springer International Publishing, 2016. s. 270-278. ISBN 978-3-319-45510-5. doi:10.1007/978-3-319-45510-5_31.
```
@inproceedings{1353405,
   author = {Medveď, Marek and Horák, Aleš},
   address = {Switzerland},
   booktitle = {Text, Speech, and Dialogue 19th International Conference, TSD 2016 Brno, Czech Republic, September 12–16, 2016 Proceedings},
   doi = {http://dx.doi.org/10.1007/978-3-319-45510-5_31},
   editor = {Sojka Petr, Horák Aleš, Kopeček Ivan, Pala Karel},
   keywords = {Question Answering; AQA; Simple Question Answering Database; SQAD; Named entity recognition},
   howpublished = {tištěná verze "print"},
   language = {eng},
   location = {Switzerland},
   isbn = {978-3-319-45510-5},
   pages = {270-278},
   publisher = {Springer International Publishing},
   title = {AQA: Automatic Question Answering System for Czech},
   url = {http://dx.doi.org/10.1007/978-3-319-45510-5_31},
   year = {2016}
}

```
 
[License](https://nlp.fi.muni.cz/en/LicenceWebCorpus)

[Publications](https://is.muni.cz/person/359226#publikace)

[Project web](https://nlp.fi.muni.cz/projekty/question_answering/)
